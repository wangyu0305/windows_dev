#include "stack.h"

stackstruct* init_stack()
{
	stackstruct* stack = (stackstruct*)malloc(sizeof(stackstruct));
	memset(stack,0,sizeof(stackstruct));
	/*stack->str = (char* *)malloc(stack_init_size * sizeof(char*));
	memset(stack->str,0,sizeof(char*));*/
	stack->top = -1;
	stack->now = stack->top;
	stack->stacksize = stack_init_size;
	return stack;
}

bool push_stack(stackstruct* stack,char* str)
{
	if (stack == NULL)
	{
		return false;
	}
    if (stack->now == stack->stacksize - 1)
    {
		return false;
		/*stack->str = (char* *)realloc(stack->str,(stack->stacksize + stack_increment) * sizeof(char*));
		if (stack->str == NULL)
		{
			return false;
		}
		stack->stacksize += stack_increment;*/
    }
	int n = 0;
	size_t m = strlen(str);
	stack->top += 1;
	stack->now += 1;

	while (*str != '\0')
	{
		stack->str[stack->now][n] = *str;
		n += 1;
		str++;
	}
	stack->str[stack->now][m] = '\0';
	
	return true;
}

bool show_stack(stackstruct* stack)
{
	int i;
	for (i = 0;i <= stack->now;i++)
	{
		printf("string:%s\n",stack->str[i]);
	}
	return true;
}

bool pop_stack(stackstruct* stack)
{
	if (stack->now == -1)
	{
		return false;
	}
	stack->str[stack->now][0] = '\0';
	stack->top -= 1;
	stack->now -= 1;
	return true;
}

bool is_empty_stack(stackstruct* stack)
{
	if (stack->now == -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

char* find_stack(stackstruct* stack,char* str)
{
	if (stack->now == -1)
	{
		return NULL;
	}
	int i = stack->top;
	while (i >= 0)
	{
		if (str_cmp(stack->str[i],str) == 0)
		{
			pop_stack(stack);
			return str;
		}
		else
		{
			i -= 1;
		}
	}
	return NULL;
	
}

bool delete_stack(stackstruct* stack)
{
	stack->now = stack->top = -1;
	stack->stacksize = 0;
	free(stack->str);
	free(stack);
	return true;
}

int str_cmp(char* strA,char* strB)
{
	if (strA == NULL || strB == NULL)
	{
		return 1;
	}
	while ((*strA != '>' && *strB != '>') && (*strA != '\0' && *strB != '\0'))
	{
		if (*strA != *strB)
		{
			return 1;
		}
		strA++;
		strB++;
	}
    return 0;
}