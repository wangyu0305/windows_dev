#include <stdio.h>
#include <stdlib.h>

#define V 2
#define MAXNUM_KEY (V * 2)
#define MAXNUM_POintER (MAXNUM_KEY + 1)
#define MAXNUM_DATA (V * 2)

#ifndef NULL
#define NULL 0
#endif

#define INVALID 0
#define FLAG_LEFT 1
#define FLAG_RIGHT 2

typedef int KEY_TYPE; 

enum NODE_TYPE
{
	NODE_TYPE_ROOT = 1,
	NODE_TYPE_INTERNAL = 2,
	NODE_TYPE_LEAF = 3,
};

class NODE 
{
public:
	NODE();
	virtual ~NODE();

	NODE_TYPE get_type(){return m_type;}
	void set_type(NODE_TYPE type){m_type = type;}

	int get_count(){return m_count;}
	void set_count(int i){m_count = i;}

	virtual KEY_TYPE get_element(int i){return 0;}
	virtual void set_element(int i,KEY_TYPE val){}

	virtual NODE* get_pointer(int i){return NULL;}
	virtual void set_pointer(int i,NODE* pointer){}

	NODE* get_father(){return m_father;}
	void set_father(NODE* father){m_father = father;}

	NODE* get_brother(int& flag);
	void delete_children();

protected:
	NODE_TYPE m_type;
	int m_count;
	NODE* m_father;

};

class INTERNALNODE :public NODE
{
public:
	INTERNALNODE();
	virtual ~INTERNALNODE();

	KEY_TYPE get_element(int i)
	{
		if ((i > 0) && (i <= MAXNUM_KEY))
		{
			return m_keys[i - 1];
		}
		else
		{
			return INVALID;
		}
	}
	void set_element(int i,KEY_TYPE val)
	{
		if ((i > 0) && (i <= MAXNUM_KEY))
		{
			m_keys[i - 1] = val;
		}
	}

	NODE* get_pointer(int i)
	{
		if ((i > 0) && (i <= MAXNUM_POintER))
		{
			return m_pointers[i - 1];
		} 
		else
		{
			return NULL;
		}
	}
    void set_pointer(int i,NODE* pointer)
	{
		if ((i > 0) && (i <= MAXNUM_POintER))
		{
			m_pointers[i - 1] = pointer;
		}
	}

	bool insert_key(KEY_TYPE val,NODE* pnode);
	bool delete_key(KEY_TYPE val);

	KEY_TYPE split(INTERNALNODE* node,KEY_TYPE val);
	bool combine(NODE* pnode);
	bool move_element(NODE* pnode);

protected:
	KEY_TYPE m_keys[MAXNUM_KEY];
	NODE* m_pointers[MAXNUM_POintER];
};

class LEAFNODE : public NODE
{
public:
	LEAFNODE();
	virtual ~LEAFNODE();

	KEY_TYPE get_element(int i)
	{
		if ((i > 0) && (i <= MAXNUM_DATA))
		{
			return m_datas[i - 1];
		} 
		else
		{
			return INVALID;
		}
	}
	void set_element(int i,KEY_TYPE val)
	{
		if ((i > 0) && (i <= MAXNUM_DATA))
		{
			m_datas[i - 1] = val;
		}
	}

	NODE* get_pointer(int i)
	{
		return NULL;
	}
	void set_pointer(int i,NODE* pointer){}

	bool insert_key(KEY_TYPE val);
	bool delete_key(KEY_TYPE val);
	KEY_TYPE split(NODE* pnode);
	bool combine(NODE* pnode);

public:
	LEAFNODE* m_prevleafnode;
	LEAFNODE* m_nextleafnode;

protected:
	KEY_TYPE m_datas[MAXNUM_DATA];
};

class BPLUSTREE
{
public:
	BPLUSTREE();
	virtual ~BPLUSTREE();
	bool search_tree(KEY_TYPE data,char* path);
	bool insert_key(KEY_TYPE data);
	bool delete_key(KEY_TYPE data);

	void clear_tree();
	void print_tree();

	BPLUSTREE* rotate_tree();
	bool check_tree();
    void print_node(NODE* pnode);
	bool check_node(NODE* pnode);

	NODE* get_root()
	{
		return m_root;
	}
    void set_root(NODE* root)
	{
		m_root = root;
	}
	int get_depth()
	{
		return m_depth;
	}
	void set_depth(int depth)
	{
		m_depth = depth;
	}

	void in_depth()
	{
		m_depth += 1;
	}
	void dec_depth()
	{
		if (m_depth > 0)
		{
			m_depth -= 1;
		}
	}

public:
	LEAFNODE* m_leafhead;
	LEAFNODE* m_leaftail;


protected:
	LEAFNODE* search_leaf_node(KEY_TYPE data);
	bool insert_internal_node(INTERNALNODE* node,KEY_TYPE key,NODE* rightson);
	bool delete_internal_node(INTERNALNODE* node,KEY_TYPE key);
	NODE* m_root;
	int m_depth;

};