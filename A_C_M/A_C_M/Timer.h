/* windows ��ʱ�� */
# ifndef _timer_f_
#define _timer_f_

#include <Windows.h>
#include <iostream>

class thread_c
{
public:
	thread_c();
	virtual ~thread_c();
	virtual void run() = 0;
	void start();
	void stop();
	bool is_stop();
protected:
	static int threadproc(LPVOID p);
private:
	bool stop_flag;
	HANDLE m_thread;
};


class timer : public thread_c
{
	typedef void (*timer_func)(void*);
	typedef timer_func timer_f;

public:
	timer():m_timer(0),m_interval(-1)
	{
		printf("timer()!\n");
	}

	void register_handle(timer_f hm,void* par)
	{
		m_timer = hm;
		parm = par;
	}

	void set_interval(int millisecond)
	{
		m_interval = millisecond;
	}

	void run()
	{
		unsigned long time_now = GetTickCount();
		unsigned long last_tick = time_now;
		while (!is_stop())
		{
			time_now = GetTickCount();
			if ((time_now - last_tick) > m_interval)
			{
				if (m_timer)
				{
					(*m_timer)(parm);
				}
				last_tick = GetTickCount();
			}
			Sleep(1);
		}
	}

	void cancel()
	{
		stop();
	}

protected:

private:
	timer_f m_timer;
	unsigned long m_interval;
	void* parm;
};

#endif