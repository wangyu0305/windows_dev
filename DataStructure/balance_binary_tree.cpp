#include<iostream>
using namespace std;

typedef struct BSTNode
{
	char data;
	int bf;
	BSTNode *lchild,*rchild;
}BSTNode,*BSTree;


//函数功能：实现对节点P的右旋处理
void R_Rotate(BSTree &p) //右转函数
{
	BSTree lc;
	lc=p->lchild; //lc指向*p的左子树根节点
	p->lchild=lc->rchild; //lc的右子树挂接为*p的左子树
	lc->rchild=p;
	p=lc; //p指向新的根节点
}

//函数功能:实现对节点P的左旋处理
void L_Rotate(BSTree &p)
{
	BSTree rc;
	rc=p->rchild; //lc指向*p的右子树根节点
	p->rchild=rc->lchild; //lc的左子树挂接为*p的左子树
	rc->lchild=p;
	p=rc; //p指向新的根节点
}




#define LH 1 //左高
#define EH 0 //等高
#define RH -1 //右高

//函数功能:实现对树T的左平衡处理
void LeftBalance(BSTree &T)
{
	BSTree lc,rd;
	lc=T->lchild; //lc指向T的左孩子
	switch(lc->bf) //检查T的左子树的平衡度,并作相应处理
	{
	case LH:T->bf=lc->bf=EH;R_Rotate(T);break;
	case RH:rd=lc->rchild; //新节点插入在T的左孩子的右子树上，做双旋处理
		switch(rd->bf) //修改T及其左孩子的平衡因子
		{
		case LH:T->bf=RH;lc->bf=EH;break;
		case EH:T->bf=lc->bf=EH;break;
		case RH:T->bf=EH; lc->bf=EH;break;
		}
		rd->bf=EH;
		L_Rotate(T->lchild); //对T的左子树作左旋处理
		R_Rotate(T); //对T作右旋平衡处理
	}
}

//函数功能:实现对树T的右旋处理
void RightBalance(BSTree &T)
{
	BSTree lc,rd;
	lc=T->rchild; //lc指向T的右孩子
	switch(lc->bf)
	{
	case RH:T->bf=lc->bf=EH;L_Rotate(T);break; //修改平衡因子，左旋处理
	case LH:rd=lc->lchild;
		switch(rd->bf) //新节点插入在T的左子树上,作双旋处理
		{
		case LH:T->bf=EH;lc->bf=RH;break;
		case EH:T->bf=lc->bf=EH;break;
		case RH:T->bf=LH; lc->bf=EH;break;
		}
		rd->bf=EH;
		R_Rotate(T->rchild); //对T的右子树右旋处理
		L_Rotate(T); //对T作左旋处理
	}
}

//函数功能:实现对元素E的插入到二叉树的操作
int InsertAVL(BSTree &T,char e,bool &taller)
{
	if(!T){ //T为空的情况。树长高
		T=(BSTree)malloc(sizeof(BSTNode));
		T->lchild=T->rchild=NULL;
		T->bf=EH;
		T->data=e;
		taller=true;
	}
	else
	{
		if(e==T->data) //树中已存在和要插入的E相等的节点,不予插入
		{
			taller=false; return 0;
		}


		else if(e<T->data) //插入到左边
		{
			if(!InsertAVL(T->lchild,e,taller)) return 0; //未插入
			if(taller)
				switch(T->bf) //检查T的平衡因子
			{
				case LH:LeftBalance(T);taller=false;break; //原本左高，左旋
				case EH: T->bf=LH;taller=true;break; //原本等高，左变高
				case RH: T->bf=EH;taller=false;break; //原本右高，变等高
			}
		}


		else //插入到右子树
		{
			if(!InsertAVL(T->rchild,e,taller)) return 0;
			if(taller)
				switch(T->bf) //检查平衡因子
			{
				case LH:T->bf=EH; taller=false;break; //原本左高，变等高
				case EH:T->bf=RH;taller=true;break; //原本等高，变右高
				case RH:RightBalance(T);taller=true;break; //原本右高，右旋处理
			}
		}

	}
	return 1;
}

//函数功能:中序打印出树的所有节点
//功能简单，不再注释
int Print(BSTree T)
{
	if(!T) return 1;
	Print(T->lchild);
	cout<<T->data<<",";//<<"平衡因子:"<<T->bf<<endl;
	Print(T->rchild);
	return 0;
}

//函数功能：先根以凹入表的形式输出树的所有节点
int PrintTree(BSTree &T,int layer)
{
	if(!T) return 1; //树为空的情况，即结束条件

	for(int i=0;i<layer;i++) //打印前面的空格
		cout<<" ";
	cout<<T->data; //打印节点信息
	layer++;
	for(int i=layer;i<20;i++) //打印后面的#号
		cout<<"#";
	cout<<endl;
	PrintTree(T->lchild,layer);
	PrintTree(T->rchild,layer);
	return 0;
}

//函数功能：查找元素e是否在平衡二叉树中，返回要查找点的双亲和本身节点指针
int SearchAVL(BSTree &T,char e)
{
	if(!T) {cout<<e<<"不在该平衡二叉树中!"<<endl<<endl;return 0;}
	if(T){
		if(T->data==e) {cout<<e<<"在平衡二叉树中."<<endl<<endl;return 0;}
		else if(T->data>e) SearchAVL(T->lchild,e);
		else SearchAVL(T->rchild,e);
	}

}

//函数功能：删除指定的T节点
int Delete(BSTree &T,char e,bool &shorter)
{
	BSTree p,q;
	e=0;
	p=T;
	if(!T->rchild) //右子树为空
	{
		T=T->lchild;
		free(p);
		shorter=true;
	}
	else if(!T->lchild) //左子树为空
	{
		T=T->rchild;
		free(p);
		shorter=true;
	}
	else //左右子树均不空
	{
		q=T->lchild;
		// Push(S,p);
		while(q->rchild!=NULL) //寻找代替节点，即左孩子的最右下节点，删除之
		{
			//Push(S,q->rchild);
			q=q->rchild;
		}
		e=q->data;
	}
	return e;
}





//函数功能：删除之后的左平衡调节
void Delete_Leftbalance(BSTree &T,bool &shorter)//删除的为左子树上的节点
{
	BSTree rc=T->rchild,ld;

	switch(rc->bf)
	{
	case LH: //先右旋后左旋
		ld=rc->lchild;
		rc->lchild=ld->rchild;
		ld->rchild=rc;
		T->rchild=rc->lchild;
		rc->lchild=T;
		switch(ld->bf)
		{
		case LH:T->bf=EH;
			rc->bf=RH;
			break;
		case EH:T->bf=rc->bf=EH;
			break;
		case RH:T->bf=LH;
			rc->bf=EH;
			break;
		}
		ld->bf=EH;
		T=rc;
		shorter=true;break;
	case EH: //单左旋
		T->rchild=rc->lchild;
		rc->lchild=T;
		rc->bf=LH;
		T->bf=RH;
		T=rc;
		shorter=EH;break;
	case RH: //单左旋
		T->rchild=rc->lchild;
		rc->lchild=T;
		rc->bf=T->bf=EH;
		T=rc;
		shorter=true;break;
	}
}


//函数功能：实现删除之后的右平衡
void Delete_Rightbalance(BSTree &T,bool &shorter)/////删除右子树上的，相当于插入在左子树上
{
	BSTree p1,p2;
	p1=T->lchild;

	switch(p1->bf)
	{
	case LH:T->lchild=p1->rchild;//单右旋
		p1->rchild=T;
		p1->bf=T->bf=EH;
		T=p1;
		shorter=true;
		break;
	case EH:T->lchild=p1->rchild;//单右旋
		p1->rchild=T;
		p1->bf=RH;
		T->bf=LH;
		T=p1;
		shorter=false;
		break;
	case RH:p2=p1->rchild; //双旋
		p1->rchild=p2->lchild;
		p2->lchild=p1;
		T->lchild=p2->rchild;
		p2->rchild=T;
		switch(p2->bf)
		{
		case LH:T->bf=RH;p1->bf=EH;break;
		case EH:T->bf=EH;p1->bf=EH;break;
		case RH:T->bf=EH;p1->bf=LH;break;
		}
		p2->bf=EH;
		T=p2;
		shorter=true;break;
	}
}

//函数功能:实现删除平衡二叉树,结构类似插入函数InsertAVL()
BSTree DeleteAVL(BSTree &T,char e,bool &shorter)
{
	int judge;/////标记
	BSTree q;

	if(!T) return NULL;
	else //树非空的情况
	{
		if(e==T->data) //根节点与之相等,直接删除
		{
			judge=Delete(T,e,shorter);

			if(judge!=0) //树高大于2时，删除根的左孩子的最右下节点
			{
				q=T;
				DeleteAVL(T,judge,shorter);
				q->data=judge;
			}
		}
		else { //如果被删的不是根节点
			if(e<T->data) //被删节点在左子树上
			{
				DeleteAVL(T->lchild,e,shorter);

				if(shorter) //树变矮了
				{
					switch(T->bf)
					{
					case LH:T->bf=EH;shorter=true;break;
					case EH:T->bf=RH;shorter=false;break;
					case RH:Delete_Leftbalance(T,shorter);shorter=true;break;
					}
				}
			}

			else //被删节点在在右子树上
			{
				DeleteAVL(T->rchild,e,shorter);

				if(shorter) //树变矮了
					switch(T->bf)
				{
					case LH:Delete_Rightbalance(T,shorter);shorter=true;break;
					case EH:T->bf=LH;shorter=false;break;
					case RH:T->bf=EH;shorter=true;break;
				}
			}
		}
	}
	return T;
}



int b_main()
{
	BSTree T=NULL;
	char e,judge='y',choice,out;
	bool taller=false;
	//SqStack S;
	//InitStack(S);

	do{
		judge='y';
		cout<<endl<<endl;

		cout<<" ***************************************"<<endl;
		cout<<" * *"<<endl;
		cout<<" * *"<<endl;
		cout<<" * 查找(s) 插入(i) 删除(d) *"<<endl;
		cout<<" * *"<<endl;
		cout<<" * 退出(q) *"<<endl;
		cout<<" ***************************************"<<endl;

		do{
			cout<<"请选择操作:";
			cin>>choice;
		}while(choice!='s'&&choice!='i'&&choice!='d'&&choice!='q');

		switch(choice)
		{
		case 'i':e='@';
			cout<<"您选择了插入。请输入你要输入的节点元素，并以“#”表示结束!"<<endl;
			cout<<endl<<"请输入你要插入的节点元素:";
			while(e!='#')
			{
				//cout<<endl<<"请输入你要插入的节点元素:";
				cin>>e;
				if(e!='#')
					InsertAVL(T,e,taller);
				//cout<<"是否继续输入(y/n)?";
				// cin>>judge;
			}
			cout<<"插入之后的平衡二叉树为:"<<endl;
			PrintTree(T,1);cout<<endl;
			break;

		case 's':while(judge=='y')
				 {

					 if(T)
					 {
						 cout<<endl<<"请输入你要查找的节点元素:";
						 cin>>e;

						 SearchAVL(T,e);
						 cout<<"是否继续查找(y/n)?";
						 cin>>judge;
					 }
					 else {cout<<"对不起,你无法进行查找操作.因为平衡二叉树是空的!"<<endl;break;}
				 }
				 break;

		case 'd':while(judge=='y')
				 {
					 if(!T) {cout<<"无法执行删除操作,因为树为空.请在树非空的情况下进行!"<<endl<<endl;break;}

					 cout<<endl<<"请输入你要删除的节点元素:";
					 cin>>e;
					 DeleteAVL(T,e,taller);
					 cout<<endl<<endl<<"删除之后的二叉树为:"<<endl;
					 PrintTree(T,1);

					 cout<<endl<<"是否继续删除(y/n)?";
					 cin>>judge;
				 }
				 break;

		case 'q':out='y';cout<<endl<<"谢谢使用！"<<endl<<" 龙行天下工作室制作出品"<<endl;;break;
		}

	}while(out!='y');

	return 0;
}

