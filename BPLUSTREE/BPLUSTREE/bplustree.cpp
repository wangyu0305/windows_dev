#include <stdio.h>
#include <stdlib.h>
#include "bplustree.h"

NODE::NODE()
{
   m_type = NODE_TYPE_LEAF;
   m_count = 0;
   m_father = NULL;
}

NODE::~NODE()
{
	delete_children();
}

NODE* NODE::get_brother(int& flag)    /*通过父节点来找兄弟节点*/
{
	int i;
	NODE* pfather = get_father();
	if (pfather == NULL)
	{
		return NULL;
	}
    NODE* pbrother = NULL;

	for (i = 1;i <= pfather->get_count() + 1;i++)
	{
		if (pfather->get_pointer(i) == this)
		{
			if (i == (pfather->get_count() + 1))
			{
				pbrother = pfather->get_pointer(i - 1);

				flag = FLAG_LEFT;
			}
			else
			{
				pbrother = pfather->get_pointer(i + 1);

				flag = FLAG_RIGHT;
			}
		}
	}
	return pbrother;
}

void NODE::delete_children()
{
	int i;
	for (i = 1;i <= get_count();i++)
	{
		NODE* pnode = get_pointer(i);
        if (pnode != NULL)
        {
			pnode->delete_children();
        }
		delete pnode;
	}
}

INTERNALNODE::INTERNALNODE()
{
	m_type = NODE_TYPE_INTERNAL;
    int i;
	for (i = 0;i < MAXNUM_KEY;i++)
	{
		m_keys[i] = INVALID;
	}
	for (i = 0;i < MAXNUM_POintER;i++)
	{
		m_pointers[i] = NULL;
	}
}

INTERNALNODE::~INTERNALNODE()
{
	int i;
	for (i = 0;i < MAXNUM_POintER;i++)
	{
		m_pointers[i] = NULL;
	}
}

bool INTERNALNODE::insert_key(KEY_TYPE val,NODE* pnode)
{
	int i = 0;
	int j = 0;
	if(get_count() >= MAXNUM_KEY)
	{
		return false;
	}
	for(i = 0;(val > m_keys[i]) && (i < m_count);i++)
	{

	}
	for (j = m_count;j > i ;j--)
	{
		m_keys[j] = m_keys[j - 1];
	}
	for (j = m_count + 1;j > i;j--)
	{
		m_pointers[j] = m_pointers[j - 1];
	}
	m_keys[i] = val;
	m_pointers[i + 1] = pnode;
	pnode->set_father(this);

	m_count++;
	return true;
}

bool INTERNALNODE::delete_key(KEY_TYPE val)
{
	int i;
	int j,k;
    for (i = 0;(val >= m_keys[i]) && (i < m_count);i++)
    {
    }
	for (j = i - 1;j < m_count - 1;j++)
	{
		m_keys[j] = m_keys[j + 1];
	}
    m_keys[j] = INVALID;
	for (k = i;k < m_count; k++)
	{
		m_pointers[k] = m_pointers[k + 1];
	}
	m_pointers[k] = NULL;
	m_count--;
	return true;
}

KEY_TYPE INTERNALNODE::split(INTERNALNODE* node,KEY_TYPE val)          //分裂中间节点
{
	int i = 0,j = 0;
	if (val > this->get_element(V) && val < this->get_element(V + 1))
	{
		for (i = V + 1;i <= MAXNUM_KEY;i++)
		{
			j++;
			node->set_element(j,this->get_element(i));
			this->set_element(i,INVALID);
		}
	
	    j = 0;
	    for (i = V + 2;i < MAXNUM_POintER;i++)
	    {
		    j++;
		    this->get_pointer(i)->set_father(node);
		    node->set_pointer(i,this->get_pointer(j));
		    this->set_pointer(i,INVALID);
	    }
	    this->set_count(V);
	    node->set_count(V);
    
	    return val;
	}
	int position = 0;
	if (val < this->get_element(V))
	{
		position = V;
	} 
	else
	{
		position = V + 1;
	}

	KEY_TYPE retkey = this->get_element(position);
	j = 0;
	for (i = position + 1;i < MAXNUM_KEY;i++)
	{
		j++;
		node->set_element(j,this->get_element(i));
		this->set_element(i,INVALID);
	}
    this->set_element(position,INVALID);
	this->set_count(position - 1);
	node->set_count(MAXNUM_KEY - position);

	return retkey;
}

bool INTERNALNODE::combine(NODE* pnode)
{
	int i;
	if (this->get_count() + pnode->get_count() + 1 > MAXNUM_DATA)
	{
		return false;
	}
	KEY_TYPE newkey = pnode->get_pointer(1)->get_element(1);
	m_keys[m_count] = newkey;
	m_count++;
	m_pointers[m_count] = pnode->get_pointer(1);
	for (i = 1;i < pnode->get_count();i++)
	{
		m_keys[m_count] = pnode->get_element(i);
		m_count++;
		m_pointers[m_count] = pnode->get_pointer(i + 1);
	}

	return true;
}

bool INTERNALNODE::move_element(NODE* pnode)               //
{
	int i,j;
	if (this->get_count() >= MAXNUM_DATA)
	{
		return false;
	}
	if (pnode->get_element(1) < this->get_element(1))
	{
		for (i = m_count;i > 0;i--)
		{
			m_keys[i] = m_keys[i - 1];
		}
		for (j = m_count + 1;j > 0;j--)
		{
			m_pointers[j] = m_pointers[j - 1];
		}
		m_keys[0] = get_pointer(1)->get_element(1);
		m_pointers[0] = pnode->get_pointer(pnode->get_count() + 1);

		pnode->set_element(pnode->get_count(),INVALID);
		pnode->set_pointer(pnode->get_count() + 1,INVALID);
	}
	else
	{
		m_keys[m_count] = pnode->get_pointer(1)->get_element(1);
		m_pointers[m_count + 1] = pnode->get_pointer(1);
		for (i = 1;i < pnode->get_count() - 1;i++)
		{
			pnode->set_element(i,pnode->get_element(i + 1));
		}
		for (j = 1;j < pnode->get_count();j++)
		{
			pnode->set_pointer(j,pnode->get_pointer(j + 1));
		}
	}
	this->set_count(this->get_count() + 1);
	pnode->set_count(pnode->get_count() - 1);

	return true;
}

LEAFNODE::LEAFNODE()
{
	int i;
	m_type = NODE_TYPE_LEAF;
    for (i = 0;i < MAXNUM_DATA;i++)
    {
		m_datas[i] = INVALID;
    }
	m_prevleafnode = NULL;
	m_nextleafnode = NULL;
}

LEAFNODE::~LEAFNODE()
{

}

bool LEAFNODE::insert_key(KEY_TYPE val)
{
	int i,j;
	if (get_count() > MAXNUM_DATA)
	{
		return false;
	}
    for (i = 0 ;(val > m_datas[i]) && (i < m_count);i++)
    {

    }
	for (j = m_count;j > i; j--)
	{
		m_datas[j] = m_datas[j - 1];
	}
	m_datas[j] = val;
	m_count++;
	return true;
}

bool LEAFNODE::delete_key(KEY_TYPE val)
{
	int i,j;
	bool found = false;
    for (i = 0;i < m_count;i++)
    {
		if (val == m_datas[i])
		{
			found = true;
			break;
		}
    }
	if (found == false)
	{
		return false;
	}
	for (j = i;j < m_count - 1;j++)
	{
		m_datas[j] = m_datas[j + 1];
	}
	m_datas[j] = INVALID;
	m_count--;
	return true;
}

KEY_TYPE LEAFNODE::split(NODE* pnode)
{
	int i,j = 0;
	for (i = 0;i < MAXNUM_DATA;i++)
	{
		j++;
		pnode->set_element(j, this->get_element(i));
        this->set_element(i,INVALID);
	}
	this->set_count(this->get_count() - j);
	pnode->set_count(this->get_count() + j);

    return pnode->get_element(1);
}

bool LEAFNODE::combine(NODE* pnode)
{
	int i;
	if ((this->get_count() + pnode->get_count()) > MAXNUM_DATA)
	{
		return false;
	}
    for (i = 1;i < pnode->get_count();i++)
    {
		this->insert_key(pnode->get_element(i));
    }
	return true;
}

BPLUSTREE::BPLUSTREE()
{
	m_depth = 0;
	m_root = NULL;
	m_leafhead = NULL;
	m_leaftail = NULL;
}

BPLUSTREE::~BPLUSTREE()
{
	clear_tree();
}

bool BPLUSTREE::search_tree(KEY_TYPE data,char* path)
{
    int i = 0;
    int offset = 0;
	if (path != NULL)
	{
		(void)sprintf(path + offset,"the search path is:");
		offset += 19;
	}
	NODE* pnode = get_root();
	while (NULL != pnode)
	{
		if (pnode->get_type() == NODE_TYPE_LEAF)
		{
			break;
		}
        for (i = 1;(data >= pnode->get_element(i)) && (i <= pnode->get_count());i++)
        {
        }
		if (path != NULL)
		{
           (void)sprintf(path + offset," %3d -->",pnode->get_element(1));
		   offset += 8;
		}
		pnode = pnode->get_pointer(i);
	}
	if (pnode == NULL)
	{
		return false;
	}
	if (path != NULL)
	{
		(void)sprintf(path + offset,"%3d",pnode->get_element(1));
		offset += 3;
	}

	bool found = false;
	for (i = 1;i < pnode->get_count();i++)
	{
		if (data == pnode->get_element(i))
		{
			found = true;
		}
	}
	if (path != NULL)
	{
		if (found == true)
		{
			(void)sprintf(path + offset,"successed!");
		}
		else
		{
			(void)sprintf(path + offset,"failed!");
		}
	}
	return found;
}

bool BPLUSTREE::insert_key(KEY_TYPE data)
{
	bool found = search_tree(data,NULL);
	if (found == true)
	{
		return false;
	}
    LEAFNODE* oldnode = search_leaf_node(data);
	if (oldnode == NULL)
    {
		oldnode = new LEAFNODE;
		m_leaftail = oldnode;
		m_leafhead = oldnode;
		set_root(oldnode);
    }
	if (oldnode->get_count() < MAXNUM_DATA)
	{
		return oldnode->insert_key(data);
	}

	LEAFNODE* newnode = new LEAFNODE;
	KEY_TYPE key = INVALID;
	key = oldnode->split(newnode);

	LEAFNODE* oldnext = oldnode->m_nextleafnode;
	oldnode->m_nextleafnode = newnode;
	newnode->m_nextleafnode = oldnext;
	newnode->m_prevleafnode = oldnode;

	if (oldnext == NULL)
	{
		m_leaftail = newnode;
	}
	else
	{
		oldnext->m_prevleafnode = newnode;
	}
    if (data < key)
    {
		oldnode->insert_key(data);
    }
	else
	{
		newnode->insert_key(data);
	}
	INTERNALNODE* father = (INTERNALNODE*)(oldnode->get_father());
	if (father == NULL)
	{
		NODE* pnode = new INTERNALNODE;
		pnode->set_pointer(1,oldnode);
		pnode->set_element(1,key);
		pnode->set_pointer(2,newnode);
		oldnode->set_father(pnode);
		newnode->set_father(pnode);
		pnode->set_count(1);
		set_root(pnode);
		return true;
	}

	bool ret = insert_internal_node(father,key,newnode);
	return ret;
}

bool BPLUSTREE::delete_key(KEY_TYPE data)
{
    int i;
	int flag = FLAG_LEFT;
	LEAFNODE* oldnode = search_leaf_node(data);
    if (oldnode == NULL)
    {
		return false;
    }
    bool success = oldnode->delete_key(data);
	if (success == false)
	{
		return false;
	}
	INTERNALNODE* father = (INTERNALNODE*)(oldnode->get_father());
	if (father == NULL)
	{
		if (oldnode->get_count() == 0)
		{
			delete oldnode;
			m_leaftail = NULL;
			m_leafhead = NULL;
			set_root(NULL);
		}
		return true;
	}

    if (oldnode->get_count() >= V)
    {
		for (i = 1;(data >= father->get_element(i)) && (i <= father->get_count());i++)
		{
			if (father->get_element(i) == data)
			{
				father->set_element(i,oldnode->get_element(1));
			}
		}
		return true;
    }

	LEAFNODE* brother = (LEAFNODE*)(oldnode->get_brother(flag));
	KEY_TYPE newdata = INVALID;
    if (brother->get_count() > V)
    {
		if (flag == FLAG_LEFT)
		{
			newdata = brother->get_element(brother->get_count());
		}
		else
		{
			newdata = brother->get_element(1);
		}

		oldnode->insert_key(newdata);
		brother->delete_key(newdata);

		if (flag == FLAG_LEFT)
		{
			for (i = 1;i <= father->get_count() + 1;i++)
			{
				if ((father->get_pointer(i) == oldnode) && (i > 1))
				{
					father->set_element(i - 1,oldnode->get_element(1));
				}
			}

		}
		else
		{
			for (i = 1;i <= father->get_count() + 1;i++)
			{
				if ((father->get_pointer(i) == oldnode) && (i > 1))
				{
					father->set_element(i - 1,oldnode->get_element(1));
				}
				if ((father->get_pointer(i) == brother) && (i > 1))
				{
					father->set_element(i - 1,brother->get_element(1));
				}
			}

		}
		return true;
    }

    KEY_TYPE newkey = NULL;
	if (flag == FLAG_LEFT)
	{
		(void)brother->combine(oldnode);
		newkey = oldnode->get_element(1);

		LEAFNODE* oldnext = oldnode->m_nextleafnode;
		brother->m_nextleafnode = oldnext;
		if (oldnext == NULL)
		{
			m_leaftail = brother;
		}
		else
		{
			oldnext->m_nextleafnode = brother;
		}
		delete oldnode;
	}
	else
	{
		(void)oldnode->combine(brother);
		newkey = brother->get_element(1);

		LEAFNODE* oldnext = brother->m_nextleafnode;
		oldnode->m_nextleafnode = oldnext;

		if (oldnext == NULL)
		{
			m_leaftail = oldnode;
		}
		else
		{
			oldnext->m_prevleafnode = oldnode;
		}
		delete brother;
	}
	return delete_internal_node(father,newkey);
}

void BPLUSTREE::clear_tree()
{
	NODE* pnode = get_root();
	if (pnode != NULL)
	{
		pnode->delete_children();
		delete pnode;
	}
	m_leafhead = NULL;
	m_leaftail = NULL;
	set_root(NULL);
}

BPLUSTREE* BPLUSTREE::rotate_tree()
{
	int i;
	BPLUSTREE* newtree = new BPLUSTREE;
	LEAFNODE* pnode = m_leafhead;
	while (pnode != NULL)
	{
		for (i = 1;i <= pnode->get_count();i++)
		{
			(void)newtree->insert_key(pnode->get_element(i));
		}
		pnode = pnode->m_nextleafnode;
	}
	return newtree;
}

bool BPLUSTREE::check_tree()
{
	LEAFNODE* this_node = m_leafhead;
	LEAFNODE* next_node = NULL;
	while (this_node != NULL)
	{
		next_node = this_node->m_nextleafnode;
		if (next_node != NULL)
		{
			if (this_node->get_element(this_node->get_count()) > next_node->get_element(1))
			{
				return false;
			}
		}
		this_node = next_node;
	}
	return check_node(get_root());
}

bool BPLUSTREE::check_node(NODE* pnode)
{
	if (pnode == NULL)
	{
		return true;
	}

	int i;
	bool ret = false;
	if ((pnode->get_count() < V) && pnode != get_root())
	{
		return false;
	}
	for (i = 1;i < pnode->get_count();i++)
	{
		if (pnode->get_element(i) > pnode->get_element(i + 1))
		{
			return false;
		}
	}
    if (pnode->get_type() == NODE_TYPE_LEAF)
    {
		return true;
    }
	for (i = 1;i <= pnode->get_count() + 1;i++)
	{
		ret = check_node(pnode->get_pointer(i));
		if (ret == false)
		{
			return false;
		}
	}
	return true;
}

LEAFNODE* BPLUSTREE::search_leaf_node(KEY_TYPE data)
{
	int i;
	NODE* pnode = get_root();
	while (pnode != NULL)
	{
		if (pnode->get_type() == NODE_TYPE_LEAF)
		{
			break;
		}
		for (i = 1;i <= pnode->get_count();i++)
		{
			if (data < pnode->get_element(i))
			{
				break;
			}
		}
		pnode = pnode->get_pointer(i);
	}

	return (LEAFNODE*)pnode;
}

bool BPLUSTREE::insert_internal_node(INTERNALNODE* node,KEY_TYPE key,NODE* rightson)
{
	if ((node == NULL) || (node->get_type() == NODE_TYPE_LEAF))
	{
		return false;
	}
	if (node->get_count() < MAXNUM_KEY)
	{
		return node->insert_key(key,rightson);
	}

	INTERNALNODE* brother = new INTERNALNODE;
	KEY_TYPE newkey = INVALID;
	newkey = node->split(brother,key);

	if (node->get_count() < brother->get_count())
	{
		node->insert_key(key,rightson);
	}
	else if (node->get_count() > brother->get_count())
	{
		brother->insert_key(key,rightson);
	}
	else
	{
		brother->set_pointer(1,rightson);
		rightson->set_father(brother);
	}

	INTERNALNODE* father = (INTERNALNODE*)node->get_father();
	if (father == NULL)
	{
		father = new INTERNALNODE;
		father->set_pointer(1,node);
		father->set_element(1,newkey);
		father->set_pointer(2,brother);
		node->set_father(father);
		brother->set_father(father);
		father->set_count(1);

		set_root(father);
		return true;
	}

	return insert_internal_node(father,newkey,brother);
}

bool BPLUSTREE::delete_internal_node(INTERNALNODE* node,KEY_TYPE key)
{
	int i;
	bool success = node->delete_key(key);
	if (success == false)
	{
		return false;
	}
	INTERNALNODE* father = (INTERNALNODE*)node->get_father();
	if (father == NULL)
	{
		if (node->get_count() == 0)
		{
			set_root(node->get_pointer(1));
			delete node;
		}
		return true;
	}
	if (node->get_count() >= V)
	{
		for (i = 1;(key >= father->get_element(i)) && (i <= father->get_count());i++)
		{
			if (father->get_element(i) == key)
			{
				father->set_element(i,node->get_element(1));
			}
		}
		return true;
	}
	int flag = FLAG_LEFT;
	INTERNALNODE* brother = (INTERNALNODE*)node->get_brother(flag);
//	KEY_TYPE newdata = INVALID;
	if (brother->get_count() > V)
	{
		node->move_element(brother);
		if (flag == FLAG_LEFT)
		{
			for (i = 1;i <= father->get_count() + 1;i++)
			{
				if ((father->get_pointer(i) == node) && (i > 1))
				{
					father->set_element(i - 1,node->get_element(1));
				}
			}
		}
		else
		{
			for (i = 1;i <= father->get_count() + 1;i++)
			{
				if ((father->get_pointer(i) == node) && (i > 1))
				{
					father->set_element(i - 1,node->get_element(1));
				}
				if ((father->get_pointer(i) == brother) && (i > 1))
				{
					father->set_element(i - 1,brother->get_element(1));
				}
			}
		}
		return true;
	}
	KEY_TYPE newkey = NULL;
	if (flag == FLAG_LEFT)
	{
		(void)brother->combine(node);
		newkey = node->get_element(1);
		delete node;
	}
	else
	{
		(void)node->combine(brother);
		newkey = brother->get_element(1);
		delete brother;
	}

	return delete_internal_node(father,newkey);
}

void BPLUSTREE::print_tree()
{
	NODE* proot = get_root();
	if (proot == NULL)
	{
		return;
	}
	NODE* p1,*p2,*p3;
	int i,l,k;
	int total = 0;
	printf("\n第一层\n");
	print_node(proot);
	total = 0;
	printf("\n第二层\n");
	for (i = 1;i < MAXNUM_POintER;i++)
	{
		p1 = proot->get_pointer(i);
		if (p1 == NULL)
		{
			continue;
		}
		print_node(p1);
		total++;
        if (total % 4 == 0)
        {
			printf("\n | ");
        }
	}
	total = 0;
	printf("\n第三层\n");
	for (i = 1;i < MAXNUM_POintER;i++)
	{
		p1 = proot->get_pointer(i);
		if (p1 == NULL)
		{
			continue;
		}
		for (l = 1;l < MAXNUM_POintER;l++)
		{
			p2 = p1->get_pointer(l);
			if (p2 == NULL)
			{
				continue;
			}
			print_node(p2);
			total++;
			if (total % 4 == 0)
			{
				printf("\n | ");
			}
		}
	}
	total = 0;
	printf("\n第四层\n");
	for (i = 1;i < MAXNUM_POintER;i++)
	{
		p1 = proot->get_pointer(i);
		if (p1 == NULL)
		{
			continue;
		}
		for (l = 1;l < MAXNUM_POintER;l++)
		{
			p2 = p1->get_pointer(l);
			if (p2 == NULL)
			{
				continue;
			}
			for (k = 1;k < MAXNUM_POintER;k++)
			{
				p3 = p2->get_pointer(k);
				if (p3 == NULL)
				{
					continue;
				}
				print_node(p3);
				total++;
				if (total % 4 == 0)
				{
					printf("\n | ");
				}
			}
		}
	}

}

void BPLUSTREE::print_node(NODE* pnode)
{
	int i;
	if (pnode == NULL)
	{
		return;
	}
	for (i = 1;i <= MAXNUM_KEY;i++)
	{
		printf("%3d \t",pnode->get_element(i));
		if (i >= MAXNUM_KEY)
		{
			printf("|");
		}
	}
}