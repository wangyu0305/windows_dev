#include <stdio.h>
#include <malloc.h>
#include <string.h>

#define stack_init_size 100
#define stack_increment 10


typedef struct stackstruct 
{
	char str[stack_init_size][20];
	int top;
	int now;
	int stacksize;
}stackstruct;

stackstruct* init_stack();

bool push_stack(stackstruct* stack,char* str);

bool show_stack(stackstruct* stack);

bool pop_stack(stackstruct* stack);

bool is_empty_stack(stackstruct* stack);

char* find_stack(stackstruct* stack,char* str);

bool delete_stack(stackstruct* stack);

int str_cmp(char* strA,char* strB);