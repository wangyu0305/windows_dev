/*AC自动机*/

/* 字典树 + KMP */

#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
using namespace std;
const int MAX=26;
typedef struct Trie_Node
{
	struct Trie_Node *fail;//失败指针
	struct Trie_Node *next[MAX];//26子节点
	int count;//单词最后一个结点的计数
}Trie;
Trie *Q[500010];//队列，用于bfs构造
char keyword[55];
char str[1000010];
int head,tail;//队列的头和尾

void insert(Trie *root,char *word)
{
	Trie *p=root;
	int i=0;
	while(word[i]!='\0')
	{
		if(p->next[word[i]-'a']==NULL)
		{
			Trie *temp=new Trie;
			for(int j=0;j<MAX;j++)
				temp->next[j]=NULL;
			temp->count=0;
			temp->fail=NULL;
			p->next[word[i]-'a']=temp;
		}
		p=p->next[word[i]-'a'];
		i++;
	}
	p->count++;
}
void build_ac(Trie *root)
{
	root->fail=NULL;
	head=tail=0;
	Q[head++]=root;
	while(head!=tail)
	{
		Trie *temp=Q[tail++];
		Trie *p=NULL;
		for(int i=0;i<MAX;i++)
		{
			if(temp->next[i]!=NULL)
			{
				if(temp==root)temp->next[i]->fail=root;
				else
				{
					p=temp->fail;
					while(p!=NULL)
					{
						if(p->next[i]!=NULL)
						{
							temp->next[i]->fail=p->next[i];
							break;
						}
						p=p->fail;
					}
					if(p==NULL)  temp->next[i]->fail=root;
				}
				Q[head++]=temp->next[i];
			}
		}
	}
}
int query(Trie *root)
{
	int i=0;
	int cnt=0;
	size_t len=strlen(str);
	Trie *p=root;
	int index;
	while(str[i])
	{
		index=str[i]-'a';
		while(p->next[index]==NULL&&p!=root)p=p->fail;
		p=p->next[index];
		if(p==NULL)p=root;
		Trie *temp=p;
		while(temp!=root)
		{
			cnt+=temp->count;
			temp->count=0;
			temp=temp->fail;
		}
		i++;
	}
	return cnt;
}
void del(Trie *root)
{
	for(int i=0;i<MAX;i++)
		if(root->next[i]!=NULL)
			del(root->next[i]);
	free(root);
}

//int main()
//{
//	//freopen("in.txt","r",stdin);
//	//freopen("out.txt","w",stdout);
//	int T;
//	int n;
//	scanf("%d",&T);
//	while(T--)
//	{
//		scanf("%d",&n);
//		Trie *root=new Trie;
//		root->count=0;
//		root->fail=NULL;
//		for(int i=0;i<MAX;i++)
//			root->next[i]=NULL;
//		for(int i=0;i<n;i++)
//		{
//			scanf("%s",&keyword);
//			insert(root,keyword);
//		}
//		build_ac(root);
//		scanf("%s",&str);
//		printf("%d\n",query(root));
//		del(root);//时间卡得紧就不删除空间
//	}
//	return 0;
//}