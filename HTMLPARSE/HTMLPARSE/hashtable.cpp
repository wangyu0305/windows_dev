#include "hashtable.h"

hashtable* create_table()
{
	hashtable* table = (hashtable*)malloc(sizeof(hashtable));
	memset(table,0,sizeof(hashtable));
	return table;
}

node* print_node(hashtable* table,int level)
{
	if (NULL == table)
	{
		return NULL;
	}

	node* pnode;
	if (NULL == (pnode = table->nodelevel[level]))
	{
		return NULL;
	}
	while (pnode != NULL)
	{
		printf("level :%d \t catagory :%d \t string : %s \n",pnode->level,pnode->tag,pnode->str);
		pnode = pnode->next;
	}
	return pnode;
}

bool insert_node(hashtable* table,int level,int tag,char* str)
{
	if (NULL == table)
	{
		return false;
	}

	node* pnode;

	if (NULL == table->nodelevel[level])
	{
		pnode = (node*)malloc(sizeof(node));
		memset(pnode,0,sizeof(node));
		pnode->level = level;
		pnode->tag = tag;
		pnode->str = str;
		table->nodelevel[level] = pnode;
		return true;
	}
	else
	{
		pnode = table->nodelevel[level];
		while (NULL != pnode)
		{
			pnode = pnode->next;
		}
		pnode->next = (node*)malloc(sizeof(node));
		memset(pnode->next,0,sizeof(node));
		pnode->next->level = level;
		pnode->next->tag = tag;
		pnode->next->str = str;
		return true;
	}
}

bool delete_table(hashtable* table)
{
	int i;
	node* pnode;
	node* phead;
	if (NULL == table)
	{
		return false;
	}
    for (i = 0;i < 20;i++)
    {
		if (NULL == (phead = table->nodelevel[i]))
		{
			continue;
		}
		while (NULL != phead->next)
		{
			pnode = phead;
			phead = phead->next;
			free(pnode);
			pnode = NULL;
		}
		free(phead);
		phead = NULL;
    }
	free(table);
	table = NULL;
	return true;
}