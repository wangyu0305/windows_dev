/* 线段树 */
/*  区间不能合并 不能更改区间大小  */
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

typedef struct seg_tree 
{
	int i , j;         //表示区间
	int recover;       //覆盖次数
    struct seg_tree *left,*right;
}seg_tree;

seg_tree* create_segtree(int i ,int j)
{
	if (i > j)
	{
		return NULL;
	}
	seg_tree* seg = NULL;
	seg = (seg_tree*)malloc(sizeof(seg_tree));
	seg->i = i;
	seg->j = j;
	seg->recover = 0;
	if ((j - i) > 1)
	{
		int midder = i + (j - i) / 2;
		seg->left = create_segtree(i,midder);
		seg->right = create_segtree(midder,j);
	}
	else
	{
		seg->left = seg->right = NULL;
	}
	return seg;
}

void insert_segtree(seg_tree* seg,int i,int j)
{
	if (i > j)
	{
		return;
	}
	if (i <= seg->i && seg->j <= j)
	{
		seg->recover++;
	} 
	else
	{
		int midder = (seg->i + seg->j) / 2;
		if (j <= midder)
		{
			insert_segtree(seg->left,i,j);
		} 
		else if (i >= midder)
		{
			insert_segtree(seg->right,i,j);
		}
		else
		{
			insert_segtree(seg->left,i,midder);
			insert_segtree(seg->right,midder,j);
		}
	}
}

//只能删除已经插入的区间
void delete_segtree(seg_tree* seg,int i,int j)
{
	if (i > j)
	{
		return;
	}
    if (i <= seg->i && j >= seg->j)
    {
		seg->recover--;
    }
	else
	{
		int midder = (seg->i + seg->j) / 2;
		if (j <= midder)
		{
			delete_segtree(seg->left,i,j);
		}
		else if (i >= midder)
		{
			delete_segtree(seg->right,i,j);
		}
		else
		{
			delete_segtree(seg->left,i,midder);
			delete_segtree(seg->right,midder,j);
		}
	}
}

void destory_segtree(seg_tree* seg)
{
	if (seg)
	{
		seg_tree* tem_t = seg;
		destory_segtree(seg->left);
		destory_segtree(seg->right);
		free(tem_t);
	}
}

void print_tree(seg_tree* seg,int dep)
{
	if (seg)
	{
		int i;
		for (i = 0;i < dep;i++)
		{
			printf("    ");
		}
		printf("[ %d, %d] cover:%d\n",seg->i,seg->j,seg->recover);
		print_tree(seg->left,dep + 1);
		print_tree(seg->right,dep + 1);
	}
}

//int main(int argc, char **argv)
//{
//	seg_tree* t_tree = create_segtree(1,20);
//	print_tree(t_tree,0);
//	insert_segtree(t_tree,2,5);
//	print_tree(t_tree,0);
//	insert_segtree(t_tree,3,9);
//	print_tree(t_tree,0);
//	delete_segtree(t_tree,7,10);
//	print_tree(t_tree,0);
//	insert_segtree(t_tree,9,18);
//	print_tree(t_tree,0);
//	destory_segtree(t_tree);
//	return 0;
//}