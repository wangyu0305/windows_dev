#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define int int

int swap(int *i, int *j);

void show(int array[], long maxlen, int begin, int end);

void quicksort(int array[], int maxlen, int begin, int end);