#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define max_size 1000

typedef struct element 
{
	int key;
}element;

element heap[max_size];

bool level(int i);

void max_varify(element heap[],int n,element item);

void min_varify(element heap[],int n,element item);

bool insert_node(element heap[],int* n,element item);