/************************************************************************/
/*                   minigrep 
   find substrings represented by regular expressions  in some other string
   or text
   
   #define DRIVER to have a command-line version complied                */
/************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#define BOL      '^'    //start of line anchor
#define EOL      '$'    //end of line anchor
#define ANY      '.'    //matches any character
#define CCL      '['    //start a character class
#define CCLEND   ']'    //end a character class
#define NCCL     '^'    //[^xyz]  负值字符集合。匹配未包含的任意字符。例如， '[^abc]' 可以匹配 "plain" 中的'p'。 
#define CLOSURE  '*'    //macth 0 or more
#define PCLOSE   '+'    //匹配前面的子表达式一次或多次。例如，'zo+' 能匹配 "zo" 以及 "zoo"，但不能匹配 "z"。+ 等价于 {1,}。
#define OPT      '?'    //匹配前面的子表达式零次或一次。例如，"do(es)?" 可以匹配 "do" 或 "does" 中的"do" 。? 等价于 {0,1}。

#define MT_BOL       (0x80 | '^')
#define MT_EOL       (0x80 | '$')
#define MT_ANY       (0x80 | '.')
#define MT_CCL       (0x80 | '[')
#define MT_OPT       (0x80 | '?')
#define MT_CLOSE     (0x80 | '*')
#define MT_PCLOSE    (0x80 | '+')

typedef unsigned char pattern;

#define  MAXPAT    128

//return values from PatternError() and MakePattern() functions
#define E_NONE    0
#define E_ILIEGAL 1
#define E_NOMEM   2
#define E_PAT     3

static int Error = E_NONE;

static pattern* DoCCL(pattern*,unsigned char*);
static int DoEscapeSeq(char**);
static int HexToBinary(int);
static int MatchOne(char**,pattern*,char*);
pattern* MakePattern(char*);
extern char* MatchString(char*,pattern*,int);
static int OctToBinary(int);
static char* PatternCmp(char*,pattern*,char*);
extern int PatternErr(void);