#include <stdio.h>
#include <stdlib.h>
#include <direct.h>

#define min(x,y) ({ typeof(x) _x = (x); typeof(y) _y = (y); (void) (&_x == &_y);  _x < _y ? _x : _y; })      //linux内核的最大最小宏定义

#define max(x,y) ({ typeof(x) _x = (x); typeof(y) _y = (y); (void) (&_x == &_y);  _x > _y ? _x : _y; })


//int main(int argc, char **argv)
//{
//	FILE* pfile;
//	FILE* pfile1;
//	char* str;
//	str = (char*)malloc(1024 * sizeof(char));
//	int ptr;
//	if ((pfile = fopen("1.mp3","rb")) == NULL)
//	{
//		printf("open file error");
//	}
//	if ((pfile1 = fopen("2.mp3","w+b")) == NULL)
//	{
//		printf("open file2 error");
//	}
//	while (1)
//	{
//		 ptr = fread(str,1,1023,pfile); // ptr = fread(str,1,1,pfile);
//		 if (ptr <= 0)
//		 {
//			 break;
//		 }
//		 fwrite(str,1,ptr,pfile1);
//	}
//    fclose(pfile);
//	fclose(pfile1);
//	int j = remove("2.mp3");
////	int k = rmdir("2.mp3");
//	return 0;
//}

/*判断机器是大端还是小端*/
typedef unsigned char *byte_pointer;

void show_bytes(byte_pointer start, int len) {
	int i;
	for (i = 0; i < len; i++) {
		printf("%.2x ", start[i]);
	} 
	printf("\n");
}
void show_int(int x) {
	show_bytes((byte_pointer)&x, sizeof(int));
}
//int main()
//{
//	int b = 1023;
//	int a[10] = {
//		1, 2, 3, 4, 5, 6, 7, 8, 9, 0
//	};
//	int *pa, *pb;
//	pa = a;
//	pb = a + 1;
//
//	printf("%d\n", &a + 1);   /*&a +1,&a是指向a[10]的指针，相当于一个二维数组。（int）(&a + 1),就是加上4 * 10个字节，这样这个问题就得到了比较好的解释了，指针加1，指针就在地址上就加上一个等于该指针指向元素的所占空间的值。*/
//	printf("%d\n", a + 1);
//	printf("%d\n", (int)(&a + 1) - (int)a);
//	
//
//	show_int(b);
//	return 0;
//}

