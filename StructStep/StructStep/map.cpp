/************************************************************************/
/*          图论        海明距离                                        */
/************************************************************************/

#include <stdio.h>


/*      海明距离  有几个不相同的位    */
int haming(char a,char b)
{
	unsigned int dst = 0;
	char ha;
	ha = a ^ b;
	while (ha)
	{
		dst++;
		ha = ha & (ha - 1);
	}
	return dst;
}

/*      深度优先查找       */

#define STACKSIZE 25

typedef struct stack stack_t;
typedef struct local local_t;
typedef struct map map_t;

struct stack
{
	int stackx[STACKSIZE];
	int stacky[STACKSIZE];
	int top;
};

struct local
{
	int path;
	int visited;
};

struct map  
{
	local_t a[STACKSIZE][STACKSIZE];
};

void map_path_init(map_t* local)
{
	int i = 0,m,n;
    for (m = 0;m < STACKSIZE;m++)
    {
		for (n = 0;n < STACKSIZE;n++)
		{
			i++;
			local->a[m][n].path = i;
			local->a[m][n].visited = 0;
		}
    }
}

void map_dfs(map_t* local,int startm,int startn,int endm,int endn)
{
	int i;
	stack_t sta;
	sta.top = 0;
    for (i = startm;i <= endm;i++)
    {
		if (local->a[i][startn].visited == 0)
		{
			local->a[i][startn].visited = 1;
			if (startn + 1 <= endn && local->a[i][startn + 1].visited == 0)
			{
				sta.stackx[sta.top] = i;
				sta.stacky[sta.top] = startn + 1;
				sta.top++;
			}
		}
    }
	while (sta.top)
	{
		sta.top--;
		map_dfs(local,sta.stackx[sta.top],sta.stacky[sta.top],endm,endn);
	}
}

void map_bfs(map_t* local,int startm,int startn,int endm,int endn)
{
	int i;
	stack_t sta;
	sta.top = 0;
	for (i = startn;i <= endn;i++)
	{
		if (local->a[startm][i].visited == 1)
		{
			local->a[startm][i].visited = 0;
			if (startm + 1 <= endm && local->a[startm + 1][i].visited == 1)
			{
				sta.stackx[sta.top] = startm + 1;
				sta.stacky[sta.top] = i;
				sta.top++;
			}
		}
	}
	while (sta.top)
	{
		sta.top--;
		map_bfs(local,sta.stackx[sta.top],sta.stacky[sta.top],endm,endn);
	}
}

int m_main(int argc, char **argv)
{
	map_t local;
	map_path_init(&local);
	map_dfs(&local,0,0,STACKSIZE - 1,STACKSIZE - 1);
	map_bfs(&local,0,0,STACKSIZE - 1,STACKSIZE - 1);

	return 0;
}