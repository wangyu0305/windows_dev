/************************************************************************/
/*       hash����            hash����                                   */
/************************************************************************/

unsigned int Whash(char* str)
{
	unsigned int h = 0;
	unsigned int x = 0;
	unsigned int ah = 33554431;
	while (*str)
	{
		h = h + ah * (*str++);
        x = h & 0xF8000000;
		h ^= (x >> 25);
	}
	return h;
}

//int main(int argc, char **argv)
//{
//	unsigned int ia = Whash("1");
//	unsigned int ib = Whash("2");
//	unsigned int ic = Whash("11");
//}

#include <memory.h>
#include <stdlib.h>
#define HASHPATH 31

typedef struct List_node Lists_t;
typedef struct Table  Table_t;
typedef struct HashTable HashTable_t;

struct List_node 
{
	int key;
    Lists_t* next;
};

struct Table
{
	Lists_t* List;
	int length;
};

struct HashTable
{
	Table_t* Hashtable[HASHPATH];
};


Table_t* create_table()
{
	Table_t* htable;
	htable = (Table_t*)malloc(sizeof(Table_t));
	htable->List = NULL;
	htable->length = 0;
	return htable;
}

Lists_t* create_node(int key)
{
	Lists_t* List = (Lists_t*)malloc(sizeof(Lists_t));
	if (List == NULL)
	{
		return NULL;
	}
	List->next = NULL;
	List->key = key;
	return List;
}

void hashtable_init(HashTable_t* hashtable)
{
	int i;
	for (i = 0; i < HASHPATH; i++)
	{
		hashtable->Hashtable[i] = NULL;
	}
}

bool insert_hashnode(Lists_t** list,int key,int& length)
{
	Lists_t* tmplist;
	if (key <= (*list)->key)
	{
		if (key == (*list)->key)
		{
			return false;
		}
		else
		{
			tmplist = create_node(key);
			if (tmplist == NULL)
			{
				return false;
			}
            tmplist->next = (*list);
			(*list) = tmplist;
			length += 1;
			return true;
		}
	}
	if ((*list)->next == NULL)
	{
		(*list)->next = create_node(key);
		if ((*list)->next == NULL)
		{
			return false;
		}
		length += 1;
		return true;
	}
	if (insert_hashnode(&(*list)->next,key,length))
	{
		return true;
	}
	return false;
}

bool insert_table(Table_t* htable,int key)
{
	if (htable->List == NULL)
	{
		htable->List = create_node(key);
		if (htable->List == NULL)
		{
			return false;
		}
		htable->length += 1;
		return true;
	}
	insert_hashnode(&htable->List,key,htable->length);

	return true;
}

bool insert_node(HashTable_t* hashtable,int key)
{
	int hkey;
	hkey = key % HASHPATH;
	if (hashtable->Hashtable[hkey] == NULL)
	{
		hashtable->Hashtable[hkey] = create_table();
		if (hashtable->Hashtable[hkey] == NULL)
		{
			return false;
		}
	}
	if (insert_table(hashtable->Hashtable[hkey],key))
	{
		return true;
	}

	return false;
}

bool find_node(Lists_t* List,int key)
{
    while (List != NULL)
    {
		if (List->key == key)
		{
			return true;
		}
		List = List->next;
    }
	
	return false;
}

bool find_hashtable(HashTable_t* hashtable,int key)
{
	int hashkey;
	Table_t* htable;
	hashkey = key % HASHPATH;
	htable = hashtable->Hashtable[hashkey];
	if (htable != NULL)
	{
		if (find_node(htable->List,key))
		{
			return true;
		}
	}
	return false;
}

bool delete_node(Table_t** htable,int key)
{
	Lists_t* putlist,*replist,*nexlist;
	int tag = 1;
	replist = (*htable)->List;
	if (*htable == NULL || (*htable)->List == NULL)
	{
		return true;
	}

	if ((*htable)->List != NULL && (*htable)->List->key == key)
	{
		putlist = (*htable)->List;
		(*htable)->List = (*htable)->List->next;
		(*htable)->length -= 1;
		free(putlist);
		putlist = NULL;
		return true;
	}

	while (replist->next != NULL)
	{
		if (replist->key == key)
		{
			Lists_t* node = replist->next;
			memcpy(replist,node,sizeof(Lists_t));
			replist->next = node->next;
			(*htable)->length -= 1;
			free(node);
			node = NULL;
			return true;
		}
        replist = replist->next;
		if (tag && replist->next->next == NULL)
		{
			nexlist = replist;
			tag = 0;
		}
	}

	if (replist->key == key)
	{
		nexlist->next = NULL;
		free(replist);
		(*htable)->length -= 1;
		replist = NULL;
		return true;
	}

	return false;
}

bool delete_hashnode(HashTable_t* hashtable,int key)
{
	int hkey;
	hkey = key % HASHPATH;
	if (hashtable->Hashtable[hkey] == NULL)
	{
		return true;
	}
	if (delete_node(&hashtable->Hashtable[hkey],key))
	{
		return true;
	}
	
	return false;
}

void delete_table(Table_t** htable)
{
	if (*htable == NULL)
	{
		return ;
	}
	Lists_t* templist;
	while ((*htable)->List != NULL)
	{
		templist = (*htable)->List;
		(*htable)->List = (*htable)->List->next;
		(*htable)->length -= 1;
		free(templist);
		templist = NULL;
	}
}

void delete_alltable(HashTable_t* hashtable)
{
	int i;
	if (hashtable == NULL)
	{
		return ;
	}
	for (i = 0; i < HASHPATH; i++)
	{
		if (hashtable->Hashtable[i] != NULL)
		{
			delete_table(&hashtable->Hashtable[i]);
			free(hashtable->Hashtable[i]);
		}
	}
}

//int h_main(int argc, char **argv)
//{
//	int i,bl;
//	int a[20] = {1,2,4,63,32,1,94,125,5,32,33,156,35,31,0,62,126,93,128,130};
//	HashTable_t hashtable;
//    hashtable_init(&hashtable);
//	for (i = 0; i < 20; i++)
//	{
//		insert_node(&hashtable,a[i]);
//	}
//	bl = find_hashtable(&hashtable,a[3]);
//
//	delete_hashnode(&hashtable,156);
//
//	delete_alltable(&hashtable);
//
//	return 0;
//}