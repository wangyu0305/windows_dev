#include "grep.h"

int grep_match(char* regrep,char* text)
{
	if (regrep[0] == '^')
	{
		return grep_matchhere(regrep + 1,text);
	}
	do 
	{
		if (grep_matchhere(regrep,text))
		{
			return 1;
		}
	} while (*text++ != '\0');

	return 0;
}

int grep_matchhere(char* regrep,char* text)
{
	if (regrep[0] == '\0')
	{
		return 1;
	}
	if (regrep[1] == '*')
	{
		return grep_matchstar(regrep[0],regrep + 2,text);
	}
	if (regrep[0] == '$' && regrep[1] == '\0')
	{
		return *text == '\0';
	}
	if (*text != '\0' && (regrep[0] == '.' || regrep[0] == *text))
	{
		return grep_matchhere(regrep + 1,text + 1);
	}
	return 0;
}

int grep_matchstar(char c,char* regrep,char* text)
{
	do 
	{
		if (grep_matchhere(regrep,text) && (c == *text))
		{
			return 1;
		}
	} while (*text != '\0' && (*text++ == c || c == '.'));
	return 0;
}

bool print_match(char* regrep,char* text)
{
	if (grep_match(regrep,text))
	{
		printf("match string is :%s\n",text);
		return true;
	}
	return false;
}