//全排列的递归实现

#include <stdio.h>  
#include <time.h>

 int n = 0;   

void swap(int *a, int *b)   
{     
	int m;    
	m = *a;     
	*a = *b;  
    *b = m;   
}   

void perm(int list[], int k, int m)          /*生成给定全排列的下一个排列 所谓一个的下一个就是这一个与下一个之间没有字典顺序中相邻的字符串。这就要求这一个与下一个有尽可能长的共同前缀，也即变化限制在尽可能短的后缀上。*/
{     
	int i;                                  //static int n;
	if(k > m)     
	{       
		for(i = 0; i <= m; i++)         
			printf("%d ", list[i]);       
		
		printf("\n");       
		n++;     
	}     
	else    
	{       
		for(i = k; i <= m; i++)       
		{         
			swap(&list[k], &list[i]);         
			perm(list, k + 1, m);        
			swap(&list[k], &list[i]);       
		}     
	}   
}   



//递归法N中取M个数的组合
void my_combine(int a[],int n,int m,int b[],const int M)
{
	int i,j;
	for (i = n;i >= m;i--)                       /// a[1..n]表示候选集，n为候选集大小，n>=m>0。
	{
		b[m - 1] = i - 1;                       /// b[1..M]用来存储当前组合中的元素(这里存储的是元素下标)，
		if (m > 1)
		{
			my_combine(a,i - 1,m - 1,b,M);        /// 求从数组a[1..n]中任选m个元素的所有组合。
		} 
		else
		{
			for (j = M - 1;j >= 0;j--)              /// 常量M表示满足条件的一个组合中元素的个数，M=m，这两个参数仅用来输出结果。
			{
				printf("%d ",a[b[j]]);
			}
			printf("\n");
		}
	}

}






//回溯法N中取M个数的组合
int my_combine(int a[],int n,int m)               /// 求从数组a[1..n]中任选m个元素的所有组合。
{
	int i;
	m = m > n ? n : m;                             /// a[1..n]表示候选集，m表示一个组合的元素个数。
    int* order = new int[m + 1];
	for (i = 0;i <= m;i++)
	{
		order[i] = i - 1;                    // 注意这里order[0]=-1用来作为循环判断标识
	}
	int count = 0;                      /// 返回所有组合的总数。
    int k = m;
	bool flag = true;                       // 标志找到一个有效组合
    while (order[0] == -1)
    {
		if (flag)
		{
			for (i = 1;i <= m;i++)
			{
				printf("%d ",a[order[i]]);            // 输出符合要求的组合
			}
            printf("\n");
			count++;
			flag = false;
		}
		order[k]++;
        if (order[k] == n)
        {
			order[k--] = 0;
			continue;
        }
		if (k < m)                                  // 更新当前位置的下一位置的数字
		{
			order[++k] = order[k - 1];
			continue;
		}
		if (k == m)
		{
			flag = true;
		}
    }
	delete[] order;
	return count;
}


//int main()   
//{     
//	time_t ntime,ltime;
//	clock_t beg,en;
//	double ti;
//	int list[] = {1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20}; 
//
//	ltime = time(NULL);
//	beg = clock();
//	perm(list, 0, 6);    
//    ntime = time(NULL);
//	en = clock();
//	ti = difftime(ltime,ntime);
//	printf("permutation runing time is %d\n",en - beg);
//	printf("total:%d\n", n); 
//	ltime = time(NULL);
//	beg = clock();
//    my_combine(list,20,4);
//	en = clock();
//	ntime = time(NULL);
//	ti = difftime(ltime,ntime);
//	printf("huisu runing time is %d\n",en - beg);
//	int tim = en - beg;
//	int b[2];
//	ltime = time(NULL);
//	beg = clock();
//    my_combine(list,20,4,b,4);
//	en = clock();
//	ntime = time(NULL);
//	ti = difftime(ltime,ntime);
//	printf("digui runing time is %d   %d\n",en - beg,tim);
//	return 0;   
//} 