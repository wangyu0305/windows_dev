/* 字典树 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int max_char_num = 26;
const int base_a = 'a';
const int base_A = 'A';

enum NODE_TYPE
{
	COMPLETED,       //“完成”是指一个字符串生成到目前为止
	UNCOMPLETED
};

typedef struct trie 
{
	enum NODE_TYPE type;
	//int NUM;            //前缀有多少个
	char c;
	struct trie* son[max_char_num];
}trie;

trie* root;           //树根什么都不存

trie* create_new_trie(char ch)
{
	trie* new_trie = (trie*)malloc(sizeof(trie));
	new_trie->c = ch;
	new_trie->type = UNCOMPLETED;
//	new_trie->NUM += 1;
	int i;
	for (i = 0;i < max_char_num;i++)
	{
		new_trie->son[i] = NULL;
	}
	return new_trie;
}

void init_trie()
{
	root = create_new_trie(' ');
}

void convert_char(char* str,int len)
{
	int i = 0;
	for (;i < len;i++)
	{
		if ((str[i] - base_A) <= 26)
		{
			str[i] = (str[i] - base_A) + base_a;
		}
	}
}

void insert_trie(char* str,int std_len)
{
	trie* pnode = root;
	convert_char(str,std_len);
	int i;
	for (i = 0;i < std_len;i++)
	{
		if (str[i] == ' ')
		{
			insert_trie(str + i + 1,std_len - i - 1);
		}
		if (pnode->son[str[i] - base_a] == NULL)
		{
			pnode->son[str[i] - base_a] = create_new_trie(str[i]);
		}
		pnode = pnode->son[str[i] - base_a];
	}
	pnode->type = COMPLETED;
}

void destory_trie(trie* tr)
{
	if (tr != NULL)
	{
		int i;
		for (i = 0;i < max_char_num;i++)
		{
			if (tr->son[i] != NULL)
			{
				destory_trie(tr->son[i]);
			}
		}
		free(tr);
		tr = NULL;
	}
}

int find_compare_word(trie* tr,char* str,int str_len)
{
	convert_char(str,str_len);
	int i = 0;
	while (i < str_len)
	{
		if (tr->son[str[i] - base_a] == NULL)
		{
			break;
		}
		tr = tr->son[str[i] - base_a];
		i++;
	}
	if ((i == str_len) && (tr->type == COMPLETED))
	{
		return 2;
	}
	else if (i == str_len)
	{
		return 1;
	}
	else 
		return 0; 
}

int find_bad_word(trie* tr,char* str,int str_len)
{
	int i = 0;
	while (tr != NULL && i == 0)
	{
		i = find_compare_word(tr,str,str_len);
		if (i >= 1)
		{
			return 1;
		}
		else
		{
			int j = 0;
			for (;j < 26;j++)
			{
				int k = 0;
				if (tr->son[j] == NULL)
				{
					continue;
				}
				k = find_bad_word(tr->son[j],str,str_len);
				if (k > 0)
				{
					i++;
					break;
				}
			}
		}
	}
	if (i > 0)
	{
		return 1;
	}
	return 0;
}

int m_main(int argc, char **argv)
{
	char ar[20];
	char* str = "probleM";
	char* ptr = "stEel prOblems";
	char* pro = "pro";
	memcpy(ar,str,7);
	init_trie();
	insert_trie(ar,7);
	memcpy(ar,ptr,14);
	insert_trie(ar,14);
	int i = find_bad_word(root,pro,3);
	if (i >= 1)
	{
		puts("find");
		exit(0);
	}
	puts("not find");
	exit(1);
}