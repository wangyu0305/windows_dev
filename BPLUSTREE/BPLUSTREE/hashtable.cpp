#include "hashtable.h"

hashtable* create_hashtable(unsigned int nodecount)
{
	hashtable* newtable = new hashtable;
//	newtable->hashnode = malloc(sizeof(tablenode) * nodecount);
	memset((void*)newtable,0,sizeof(hashtable));
	return newtable;
}

tablenode* search_table(hashtable* table,unsigned int nodecount,int data,char* str)
{
	tablenode* newnode;
	newnode = NULL;
	if (table == NULL)
	{
		return NULL;
	}
	if ((newnode = table->hashnode[data % nodecount]) == NULL)
	{
		return NULL;
	}
	while (newnode)
	{
		if (0 == (strcmp(str,newnode->str)))
		{
			return newnode;
		}
        newnode = newnode->next;
	}
    return NULL;
}

bool insert_table(hashtable* table,unsigned int nodecount,int data,void* buffer,char* str)
{
	tablenode* newnode;
	newnode = NULL;
	if (table == NULL)
	{
		return false;
	}
	if ((table->hashnode[data % nodecount]) == NULL)
	{
		newnode = new tablenode;
		newnode->data = data;
		newnode->str = str;
		newnode->buffer = buffer;
		newnode->next = new tablenode;
		newnode->next = NULL;
		table->hashnode[data % nodecount] = newnode;
		return true;
	}
	if (search_table(table,nodecount,data,str) != NULL)
	{
		return false;
	}

	newnode = table->hashnode[data % nodecount];
	while (newnode != NULL)
	{
        newnode = newnode->next;
	}

	memset(newnode->next,0,sizeof(tablenode));
	newnode->next->data = data;
	newnode->next->str = str;
	newnode->next->buffer = buffer;
	newnode->next->next = new tablenode;
	newnode->next->next = NULL;
	return true;
	
}

bool clearall_table(hashtable* table,unsigned int nodecount)
{
	if (table == NULL)
	{
		return true;
	}
	unsigned int i;
	tablenode* node;
	tablenode* tempnode;
	for (i = 0;i < nodecount;i++)
	{
		if ((node = table->hashnode[i]) != NULL)
		{
			while (node)
			{
				tempnode = node;
				node = node->next;
                delete tempnode;
				tempnode = NULL;
			}
			delete node;
			node = NULL;
		}
	}
	delete table;
	table = NULL;
	return true;
}

bool delete_tablenode(hashtable* table,unsigned int nodecount,int data,char* str)
{
	tablenode* headnode;
	tablenode* newnode;

	if (table == NULL || (table->hashnode[data % nodecount]) == NULL)
	{
		return true;
	}
    if ((newnode = search_table(table,nodecount,data,str)) == NULL)
    {
		return true;
    }
	if (newnode == (table->hashnode[data % nodecount]))
	{
		table->hashnode[data % nodecount] = newnode->next;
        delete newnode;
		return true;
	}
	headnode = table->hashnode[data % nodecount];
	while (headnode != newnode)
	{
		headnode = headnode->next;
	}
	headnode->next = newnode->next;
	delete newnode;
	return true;
}

bool print_node(hashtable* table,unsigned int nodecount,int data,char* str)
{
	tablenode* newnode;
	tablenode* headnode;
	if (table == NULL)
	{
		return false;
	}
    if ((newnode = search_table(table,nodecount,data,str)) == NULL)
    {
		return false;
    }
	headnode = table->hashnode[data % nodecount];
	while (headnode != NULL)
	{
		if (newnode == headnode)
		{
			printf("node :%d ,string :%s\n",newnode->data,newnode->str);
			return true;
		}
		headnode = headnode->next;
	}
	return false;
}

void print_table(hashtable* table,unsigned int nodecount)
{
	if (table == NULL)
	{
		printf("empty table!\n");
	}
	unsigned int i;
	tablenode* newnode;
	for (i = 0;i < nodecount;i++)
	{
		if (table->hashnode[i] == NULL)
		{
			continue;
		}
        newnode = table->hashnode[i];
		while (newnode != NULL)
		{
			printf("node :%d ,string :%s\n",newnode->data,newnode->str);
			newnode = newnode->next;
		}
	}
}