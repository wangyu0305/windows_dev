#include "hash.h"
#include "hashtable.h"
#include <stdio.h>
#include <iostream>

using namespace std;

typedef struct nodeinfo 
{
	unsigned int hashinfo;
	int data;
	char* str;
}nodeinfo;

nodeinfo* get_hashinfo(char* node)
{
	int get_data(char* str);
	if (node == NULL)
	{
		return NULL;
	}
	nodeinfo* info = new nodeinfo;
	info->data = get_data(node);
	info->str = node;
	info->hashinfo = elf_hash(node,info->data);
	return info;
}

int get_hashsize(int data)
{
	int nodecount = 0;
	int i;
	if (data == 0)
	{
		return 0;
	}
	for (i = 0;i < 28;i++)
	{
		if (data <= hashsize[i])
		{
			nodecount = hashsize[i];
			return nodecount;
		}
	}
    return nodecount;
}

int get_data(char* str)
{
	if (str == NULL)
	{
		return 0;
	}
	int data;
	data = str[0] - '0';
	return data;
}

int in_put()
{
	int data;
	unsigned int nodecount;
	int input = 10;
	char str[20];
	nodeinfo* info;
	hashtable* table;
	tablenode* node;
	while (input != 0)
	{
		printf("\n\n");
		printf("    *******************************************************************\n");
		printf("    *           请选择相应功能。                *\n");
		printf("    *           1。建立链表                                 *\n");
		printf("    *           2。在链表中查找一个数；                               *\n");
		printf("    *           3。在链表中插入一个数；                               *\n");
		printf("    *           4。在链表中删除一个数；                               *\n");
		printf("    *           5  删除全部节点；                                         *\n");
		printf("    *           6。显示打印整个链表；                                     *\n");
		printf("    *           7。显示找到的节点；                                     *\n");
		printf("    *           0。退出程序；                                         *\n");
		printf("    *******************************************************************\n");
		printf("\n    您的选择是：");
		cin>>input;
		if (input < 0 || input > 100)
		{
			return 1;
		}
		switch (input)
		{
		case 1:
			printf("输入一个数\n");
			cin>>input;
			nodecount = get_hashsize(input);
			table = create_hashtable(nodecount);
			break;
		case 2:
			printf("输入要查找的字符\n");
			cin>>str;
			data = get_data(str);
            node = search_table(table,nodecount,data,str);
			printf("data:%d string:%s\n",node->data,node->str);
			break;
		case 3:
			printf("输入要插入的字符\n");
			cin>>str;
			data = get_data(str);
			info = get_hashinfo(str);
			if (insert_table(table,nodecount,data,(void*)info,str) == true)
			{
				printf("插入成功\n");
			}
			else
			{
				printf("插入失败\n");
			}
			break;
		case 4:
			printf("输入要删除的字符\n");
			cin>>str;
			data = get_data(str);
			if (delete_tablenode(table,nodecount,data,str) == true)
			{
				printf("删除成功\n");
			} 
			else
			{
				printf("删除失败\n");
			}
			break;
		case 5:
			if (clearall_table(table,nodecount) == true)
			{
				printf("删除成功\n");
			} 
			else
			{
				printf("删除失败\n");
			}
			break;
		case 6:
			print_table(table,nodecount);
			break;
		case 7:
			printf("输入要显示查找的字符\n");
			cin>>str;
			data = get_data(str);
			print_node(table,nodecount,data,str);
			break;
		case 0:
			return 0;
		default:
			return 0;
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	in_put();
    return 0;
}